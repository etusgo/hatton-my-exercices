import {my_display_alpha} from "./exercice-1.js";

export const my_display_alpha_reverse = () => {
    let a = my_display_alpha()
    let b = ""
    for (let i = a.length - 1; i >= 0; --i){
        b += a[i];
    }
    return b;
};
