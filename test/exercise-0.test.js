import {my_sum} from '../day-1/exercise-0.js';
import {assert} from 'chai';

describe("unit test", function() {
	it("should return the sum", function() {
		let sum = my_sum(1, 1);
		assert.equal(sum, 2);
	});
});
